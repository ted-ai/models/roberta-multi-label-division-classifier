import json
from pathlib import Path
from typing import Dict

import torch
from transformers import AutoTokenizer, AutoModelForSequenceClassification

MODEL_PATH = Path('/opt') / 'ml' / 'model' / 'roberta-cpv-multilabel'  # Path where all your model(s) live in

CPV_MAPPING = {
    "03": "Agricultural, farming, fishing, forestry and related products",
    "09": "Petroleum products, fuel, electricity and other sources of energy",
    "14": "Mining, basic metals and related products",
    "15": "Food, beverages, tobacco and related products",
    "16": "Agricultural machinery",
    "18": "Clothing, footwear, luggage articles and accessories",
    "19": "Leather and textile fabrics, plastic and rubber materials",
    "22": "Printed matter and related products",
    "24": "Chemical products",
    "30": "Office and computing machinery, equipment and supplies except furniture and software packages",
    "31": "Electrical machinery, apparatus, equipment and consumables; Lighting",
    "32": "Radio, television, communication, telecommunication and related equipment",
    "33": "Medical equipments, pharmaceuticals and personal care products",
    "34": "Transport equipment and auxiliary products to transportation",
    "35": "Security, fire-fighting, police and defence equipment",
    "37": "Musical instruments, sport goods, games, toys, handicraft, art materials and accessories",
    "38": "Laboratory, optical and precision equipments (excl. glasses)",
    "39": "Furniture (incl. office furniture), furnishings, domestic appliances (excl. lighting) and cleaning products",
    "41": "Collected and purified water",
    "42": "Industrial machinery",
    "43": "Machinery for mining, quarrying, construction equipment",
    "44": "Construction structures and materials; auxiliary products to construction (excepts electric apparatus)",
    "45": "Construction work",
    "48": "Software package and information systems",
    "50": "Repair and maintenance services",
    "51": "Installation services (except software)",
    "55": "Hotel, restaurant and retail trade services",
    "60": "Transport services (excl. Waste transport)",
    "63": "Supporting and auxiliary transport services; travel agencies services",
    "64": "Postal and telecommunications services",
    "65": "Public utilities",
    "66": "Financial and insurance services",
    "70": "Real estate services",
    "71": "Architectural, construction, engineering and inspection services",
    "72": "IT services: consulting, software development, Internet and support",
    "73": "Research and development services and related consultancy services",
    "75": "Administration, defence and social security services",
    "76": "Services related to the oil and gas industry",
    "77": "Agricultural, forestry, horticultural, aquacultural and apicultural services",
    "79": "Business services: law, marketing, consulting, recruitment, printing and security",
    "80": "Education and training services",
    "85": "Health and social work services",
    "90": "Sewage-, refuse-, cleaning-, and environmental services",
    "92": "Recreational, cultural and sporting services",
    "98": "Other community, social and personal services",
}


class ModelService(object):
    tokenizer = None
    model = None
    id2label = None

    @classmethod
    def get_tokenizer(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.tokenizer is None:
            cls.tokenizer = AutoTokenizer.from_pretrained(MODEL_PATH, local_files_only=True)
        return cls.tokenizer

    @classmethod
    def get_model(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.model is None:
            cls.model = AutoModelForSequenceClassification.from_pretrained(MODEL_PATH)
        return cls.model

    @classmethod
    def get_id2label(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.id2label is None:
            cls.id2label = json.loads((MODEL_PATH / 'config.json').read_text())["id2label"]
        return cls.id2label

    @classmethod
    def predict(cls, input):
        """For the input, do the predictions and return them."""
        tokenizer = cls.get_tokenizer()
        model = cls.get_model()
        id2label = cls.get_id2label()
        encoded_input = tokenizer(input, return_tensors='pt', padding=True, truncation=True)
        results = model(**encoded_input)
        sigmoid = torch.nn.Sigmoid()
        probs = sigmoid(results.logits.squeeze().cpu())
        return [(id2label[str(idx)], float(score)) for idx, score in enumerate(probs) if score >= 0.5]


def predict(json_input: Dict):
    """
    Prediction given the request input
    :param json_input: [dict], request input
    :return: [dict], prediction
    """
    print(f"Predict for JSON input: {json_input}")

    title = json_input.get("title", "")
    description = json_input.get("description", "")
    if not title and not description:
        raise Exception("No title and description provided")
    preprocessed_title_description = f"{title} {description}"
    predictions = ModelService.predict(preprocessed_title_description)
    results = [
        {
            "cpv_number": cpv_number,
            "cpv_text": CPV_MAPPING.get(cpv_number),
            "score": score,
        }
        for (cpv_number, score) in predictions
    ]
    print(f"Return results: {results}")
    return {
        "predictions": results
    }
