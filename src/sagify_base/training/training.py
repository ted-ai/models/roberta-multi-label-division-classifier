from __future__ import print_function


def train(_input_data_path: str, _model_save_path: str, _hyperparams_path: str = None):
    """
    The function to execute the training.

    :param _input_data_path: [str], input directory path where all the training file(s) reside in
    :param _model_save_path: [str], directory path to save your model(s)
    :param _hyperparams_path: [optional[str], default=None], input path to hyperparams json file.
    Example:
        {
            "max_leaf_nodes": 10,
            "n_estimators": 200
        }
    """
    raise NotImplementedError("Training not supported as the model has been trained manually")
